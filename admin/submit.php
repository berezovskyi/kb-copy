<?php

/**
 * @author Andrew Berezovskiy
 * @copyright 2009
 */

include "../config.php";

$title = trim($_POST['question']);
$answer = trim(nl2br($_POST['answer']));
if(count($_FILES)>0) {
	$files = $_FILES;
	$size = count($files['file']['error']);
	$fup = array();
	for($i=0; $i<$size;$i++) {
		if($files['file']['error'][$i] == 0) {
			$fup[] = $i;
		}
}
}
else {
	$size = 0;
	$fup = array();
}

if(!empty($title)) {
	$query = "INSERT INTO questions(title,answer) VALUES('".sqlite_escape_string($title)."', '".
	sqlite_escape_string($answer)."')";
	//echo $query;
	$res = sqlite_query($query, $handle);
	$id = sqlite_last_insert_rowid($handle);
	$files_embed = array();
	for($i=0; $i<count($fup); $i++) {
		$query = "INSERT INTO data(quid,name,contents, mime) VALUES($id, '".sqlite_escape_string($files['file']['name'][$fup[$i]])."', '".sqlite_escape_string(file_get_contents($files['file']['tmp_name'][$fup[$i]]))."', '".sqlite_escape_string($files['file']['type'][$fup[$i]])."' )";	
		//echo $query; 
		$res = sqlite_query($query, $handle);
		$answer = str_replace("{file{$i}}", "<img src='get.php?id=".sqlite_last_insert_rowid($handle)."'>", $answer);
	}
	$query = "UPDATE questions SET answer='".sqlite_escape_string($answer)."' WHERE id=$id";
	if(sqlite_query($query, $handle)) {
		header("Location: view.php?id=$id");
	}
}

/*
$query = "SELECT id, title FROM questions LIMIT 1000";
$res = sqlite_query($query, $handle);
*/
sqlite_close($handle);
?>