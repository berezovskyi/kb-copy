<?php

/**
 * @author Andrew Berezovskiy
 * @copyright 2009
 */

$_GET['name'] = "math";
$_GET['overwrite'] = true;
if(empty($_GET['name'])) {
	
	// Void the create form
}
else {
	$filename = "../".$_GET['name'].".sldb";
	if(file_exists($filename)) {
		if($_GET['overwrite']) {
			unlink($filename);
		}	
	}
	$handle = sqlite_open($filename);
	
	$query = "CREATE TABLE questions (
  id INTEGER UNIQUE PRIMARY KEY, 
  title CHAR(128), 
  answer CHAR(8192));";
	sqlite_exec($query, $handle);
	
	$query = "CREATE TABLE data (
  id INTEGER UNIQUE PRIMARY KEY, 
  quid INTEGER, 
  name CHAR(128), 
  contents BLOB,
  mime CHAR(128));";
	sqlite_exec($query, $handle);
	
	sqlite_close($handle);
}
?>